# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 08:47:05 2016

@author: wgresov
edited by Dhanraj Akula 
"""

import numpy as np

def PathToTrajectory(relPose, Path, nodes):
#function trajectory = PathToTrajectory(relPose,Path,nodes) 
#%This code takes the output path from the A*, the node map, and the
#%relative pose of the vessel, and outputs a trajectory for the low level
#%controller.
# 
#%% Initialize output
#trajectory = zeros(8,2);
    # trajectory = np.zeros((8, 2), dtype=np.int) 
    trajectory = np.zeros((8, 2)) 
#Determine grid points
#%Find the closest location in the path to the current point
    Path_without_nan=np.delete(Path,np.where( np.isnan(Path[:,0]) ),axis=0).astype(int)
    
    # print nodes[0,Path_without_nan[:,0],Path_without_nan[:,1]]

    # print 'dist',


    closestIdx =np.argmin( abs(nodes[0,Path_without_nan[:,0],Path_without_nan[:,1]] - relPose[0]) + abs(nodes[1,Path_without_nan[:,0],Path_without_nan[:,1]] - relPose[1]) )
    

    # np.argmin(abs(Path_without_nan[:, 0] - relPose[0]) + abs(Path_without_nan[:, 1] - relPose[1]))

#%Initial point is next index, as closest could be behind the vessel

    initIdx = closestIdx+1

#%Find how many points there are past the initial in the path
    num_remaining = Path[:,1].size - initIdx - 1
   
#%Find the last path point to pass
    # num_points = min([num_remaining, 8-(Path_without_nan[:,0].size)])
    
    if Path_without_nan[:,0].size < 8:
        num_points = min([num_remaining, Path_without_nan[:,0].size])
    else:
        num_points = min([num_remaining, 8])

    lastIdx = initIdx + num_points
#%Grab points
    
    points = Path[initIdx : lastIdx, :].astype(int)
    
#%Find the NED coordinates of those grid points, and write output
    
    trajectory[ : num_points, 0] = nodes[0,points[:,0],points[:,1]]
    trajectory[ : num_points, 1] = nodes[1,points[:,0],points[:,1]]
    trajectory=np.delete(trajectory,np.where( (trajectory[:,0])==0),axis=0)
    
    return {'trajectory':trajectory}