__author__ = 'Dhanraj Akula'
__date__ = 'Oct 12 2016'


import numpy as np

#Supplying the inputs to the program 

#Providing the NEDCorners 
#corners must be ordered .First row is the bottom left coordinate 
#then bottom right is second,followed by top left and finally top right 
#only need top and bottom,will segment vertically in loop 

#the vessel position is given by X,Y,Psi

#Providing the exit gate position (specified in NEDcordinates same as the NEDcorners)

#providing the landmarks matrix which specifies distance from the vessel in the 
#first colum and angle in the second colum 

def MapGrid(exitGate,NEDcorners,X,Y,Psi,landmarks):
	#specifying the the grid size
	# cells_vert=25
	# cells_horiz=10

	cells_vert=25
	cells_horiz=10
	#creat a matrix top_edge which holds the points on the top_edge
	num_horiz=np.matrix(range(cells_horiz))
	#y1 is a dummy variable used in calculation of top_edge
	y1=np.matrix.transpose(num_horiz)*(np.matrix([(NEDcorners[3,0]-NEDcorners[2,0])/float(cells_horiz-1),
		(NEDcorners[3,1]-NEDcorners[2,1])/float(cells_horiz-1)]))
	top_edge=y1+(np.matrix.transpose(np.matrix(np.ones(cells_horiz)))*np.matrix(NEDcorners[2,:]))
	#creat a matrix bottom_edge which holds the points on the bottom_edge
	#y2 is a dummy variable used in calculation of bottom_edge
	y2=np.matrix.transpose(num_horiz)*(np.matrix([(NEDcorners[1,0]-NEDcorners[0,0])/float(cells_horiz-1),
		(NEDcorners[1,1]-NEDcorners[0,1])/float(cells_horiz-1)]))
	bottom_edge=y2+(np.matrix.transpose(np.matrix(np.ones(cells_horiz)))*np.matrix(NEDcorners[0,:]))
	 

	#defining a matrix of zeros with shape as grid size 
	nodes=np.zeros((2,cells_vert,cells_horiz))

	#iterating and calculating the points of each points in the node matrix(Grid)
	for i in range(cells_vert):
		for j in range(cells_horiz):
			nodes[0,i,j]=(i*(top_edge[j,0]-bottom_edge[j,0])/float(cells_vert-1))+bottom_edge[j,0]
			nodes[1,i,j]=(i*(top_edge[j,1]-bottom_edge[j,1])/float(cells_vert-1))+bottom_edge[j,1]

	#Find the closest grid to the vessel
	#finding the shortest distance between the given vessel position and 
	#the point in the grid and storing the position in relPose

	clist=np.min(abs(nodes[0,:,:]-X)+abs(nodes[1,:,:]-Y),axis=0)
	rlist=np.argmin(abs(nodes[0,:,:]-X)+abs(nodes[1,:,:]-Y),axis=0)
	col=np.argmin(clist,axis=0)
	row=rlist[col]
	relPose=[row,col]
	#Find the closes grid point to the exit gate 
	#Finding the shortest distance between the given exitGate position and 
	#the point in the grid,Then storing the position in targetPt

	clist=np.min(abs(nodes[0,:,:]-	exitGate[0,0] )+abs(nodes[1,:,:]-exitGate[0,1] ),axis=0)
	rlist=np.argmin(abs(nodes[0,:,:]-exitGate[0,0])+abs(nodes[1,:,:]-exitGate[0,1]),axis=0)
	col=np.argmin(clist,axis=0)
	row=rlist[col]
	targetPt=[row,col]

	#populate Obstacle grid 

	#Converting the landmarks to NED coordinates
	
	if landmarks.size>0:
		theta=(Psi+landmarks[:,1])*(3.14/float(180))
		NEDlandmarks_x=np.matrix( X+( np.multiply( landmarks[:,0],np.cos(theta) ) ) )
		NEDlandmarks_y=np.matrix( Y+( np.multiply( landmarks[:,0],np.sin(theta) ) ) )

		#Finding the limits on north and east coordinates to remove landmarks from the map
		#that are outside the map.
		Nmax=max(NEDcorners[:,0])
		Nmin=min(NEDcorners[:,0])
		Emax=max(NEDcorners[:,1])
		Emin=min(NEDcorners[:,1])

		#Comparing the the x(north) coordinate of the NEDLandmaks cordinate with the max
		#and min value of the NEDcorners of the y cordinates and construting a boolena array 
		#with true if the condition is satisfied else false 
		b_NEDlandmark_Nmax=NEDlandmarks_x < Nmax 
		b_NEDlandmark_Nmin=NEDlandmarks_x > Nmin
		#Comparing the the y(east) coordinate of the NEDLandmaks cordinate with the max
		#and min value of the NEDcorners of the y cordinates and construting a boolena array 
		#with true if it condition is satisfied else false 

		b_NEDlandmark_Emax=NEDlandmarks_y<Emax
		b_NEDlandmark_Emin=NEDlandmarks_y>Emin

		#checking if the North cordinates is valid 
		Nvalid=np.all([b_NEDlandmark_Nmax,b_NEDlandmark_Nmin],axis=0)

		#Checking if the East cordinate are valid 
		Evalid=np.all([b_NEDlandmark_Emax,b_NEDlandmark_Emin],axis=0)

		#Comapring the Nvalid and the Evalid array to find the the index which can be removed

		#NEbounded contains the matrix of booleans where false indicates that the corresponding 
		#NED landmark cordinate is out of bound  
		#NEbounded=np.matrix(np.any([Nvalid,Evalid],axis=0))
		#NEbounded=np.matrix(np.any([Nvalid,Evalid],axis=0))
		NEbounded=np.matrix(np.all([Nvalid,Evalid],axis=0))
		#deleting the respective element of the NEDlandmarks_x,NEDlandmarks_y
		#if the landmark is out of range 
		NEDlandmarks_x=np.matrix(np.delete(NEDlandmarks_x,np.where(NEbounded == 0)[0]))
		NEDlandmarks_y=np.matrix(np.delete(NEDlandmarks_y,np.where(NEbounded == 0)[0])) 


		#Initializing obstacle grid 
		obGrid=np.zeros((cells_vert,cells_horiz))

		#relaive coordinates for padding 
		neighbors_rel=np.matrix([[1,-1],[1,0],[1,1],[0,1],[-1,1],[-1,0],[-1,-1],[0,-1]])
		#finding the closest location for the each of the lank mark in the obstacle 

		for k in range(NEDlandmarks_x.size):
			#finding the nearest point in the grid to the landmark position 
			clist=np.min(abs(nodes[0,:,:]-NEDlandmarks_x[0,k])+abs(nodes[1,:,:]-NEDlandmarks_y[0,k]),axis=0)
			rlist=np.argmin(abs(nodes[0,:,:]-NEDlandmarks_x[0,k])+abs(nodes[1,:,:]-NEDlandmarks_y[0,k]),axis=0)
			col=np.argmin(clist,axis=0)
			row=rlist[col]
			#mark the land mark position as -1 in the obstacle grid 
			obGrid[row,col]=-1
			#finding the cordinates of the neighbour cordinates of the Landmark 
			neighbors_abs=neighbors_rel + (np.ones((8,1)))*np.matrix([row,col])
			#Once the neighbours cordinartes are found we need to discard all the cordinates 
			#which is out of the grid size 
			#removing the neighbours cordinates which are out of size of obGrid 
			#using dummy variables b1,b2,b3,b4 to store the comparition results for checking 
			#checking the valid range for index of the neighbors_abs 
			b1=neighbors_abs[:,0]>=0
			b2=neighbors_abs[:,0]<cells_vert
			b3=neighbors_abs[:,1]>=0
			b4=neighbors_abs[:,1]<cells_horiz
			neighbors_invalidX=np.all([b1,b2],axis=0)
			neighbors_invalidY=np.all([b3,b4],axis=0)
			#finding a boolena matrix which has false if the index is not valid 
			neighbors_invalid=np.all([neighbors_invalidX,neighbors_invalidY],axis=0)
			#removing the elements from the matrix neighbors_abs which has the 
			#index of the position of the neighbours of the Landmark 
			neighbors_abs=np.matrix(np.delete(neighbors_abs,np.where(neighbors_invalid == 0)[0][:],axis=0))
			
			neighbors_abs=np.reshape(neighbors_abs,((neighbors_abs.size)/2,2)).astype(int)
			
			#Checking if the Landmark neighbour and the Landmark coincide
			#Structing a matrix of boolean number named neighbour_obstacles which contains 
			#true if the Landmark position and the Neighbour position coincide 
			neighbour_obstacles=obGrid[neighbors_abs[:,0],neighbors_abs[:,1]]==-1
			
			#Removing the neighbour position if it coincides with Landmark position 
			neighbors_abs=np.matrix(np.delete(neighbors_abs,np.where(neighbour_obstacles==1)[0][:],axis=0))
			neighbors_abs=np.reshape(neighbors_abs,((neighbors_abs.size)/2,2)).astype(int)
		
			#Pad all the neighbouring position to the landmark as 2 and increment the value if 
			#the same position is repeated 
			obGrid[neighbors_abs[:,0].astype(int),neighbors_abs[:,1].astype(int)]+=2
	else:
		obGrid=np.zeros((cells_vert,cells_horiz))	
	
	#printing the obGrid
	return {'nodes':nodes,'obGrid':obGrid.astype(int),'relPose':relPose,'targetPt':targetPt}





	