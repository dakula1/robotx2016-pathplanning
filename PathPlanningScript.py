__author__ = 'Dhanraj Akula'
__date__ = 'Oct 15 2016'


import numpy as np
import matplotlib.pyplot as plt
import map_grid
import astar
import PathToTrajectory


#Supplying the inputs to the program map_grid.py 

#Providing the NEDCorners 
#corners must be ordered .First row is the bottom left coordinate 
#then bottom right is second,followed by top left and finally top right 
#only need top and bottom,will segment vertically in loop 

NEDcorners=np.matrix([[2,0],[8,1],[4,10],[12,12]])
#NEDcorners=np.matrix([[3,0],[8,1],[3,10],[6,12]])
#print type(NEDcorners)
#the vessel position is given by X,Y,Psi
#let the vessel position be 
X=4
Y=2
#Providing the exit gate position (specified in NEDcordinates same as the NEDcorners)
# exitGate=np.matrix([9,9])
exitGate=np.matrix([7,10])
# exitGate=np.matrix([9.8,7.6])
#print type(exitGate)
#providing the landmarks matrix which specifies distance from the vessel in the 
#first colum and angle in the second colum 
# landmarks=np.matrix([[4.1231,90],[4.5,88],[30,25],[5,10],[5,40],[5,20],[8,60]])
landmarks=np.matrix([[3,90],[6,70],[70,80],[30,80],[5,50],[8,45],[3,30]])
# landmarks=np.matrix([])
# landmarks=np.matrix([[3,90],[8,45],[3,30]])
#print type(landmarks)
#landmarks=np.matrix([[4.1231,90],[30,25],[6,70],[6,60]])
#providing an arbitary value for Psi
Psi=0
# cells_vert=25
# cells_horiz=10

cells_vert=25
cells_horiz=10




output_mapGrid=map_grid.MapGrid(exitGate,NEDcorners,X,Y,Psi,landmarks)
nodes=output_mapGrid['nodes']
obGrid=output_mapGrid['obGrid']
relPose=output_mapGrid['relPose']
targetPt=output_mapGrid['targetPt']


Path=astar.astar(nodes,obGrid,relPose,targetPt)['Path']

check_path_exists=np.delete(Path,np.where(np.isnan(Path[:,0])),axis=0).size 
if check_path_exists>0:
	trajectory=PathToTrajectory.PathToTrajectory(relPose, Path, nodes)['trajectory']
# trajectory=PathToTrajectory.PathToTrajectory([4.5,2.4], Path, nodes)['trajectory']
# trajectory=PathToTrajectory.PathToTrajectory([6.5,6.5], Path, nodes)['trajectory']



# ploting the various data 
# ploting the data 

index_of_Path=np.delete(Path,np.where( np.isnan(Path[:,0]) ),axis=0).astype(int)

#obtaining the position of the NEDcordinates from the obstacle grid. 
list_neighbors=np.where(obGrid>0)
list_obstacls=np.where(obGrid==-1)




#defining a variable xx just to print convert the n by m matrix into a linear for 
#which helps in prining the points on graph easy 
xx=np.reshape(nodes,(1,(2*cells_vert*cells_horiz)))
#ploting the grid in black 
plt.plot(xx[0,0:cells_vert*cells_horiz], xx[0,cells_vert*cells_horiz:2*cells_vert*cells_horiz],'o',label='Nodes',color='black')
#plottting the nearest point in the grid to the real position of the vessel 
plt.plot(nodes[0,relPose[0],relPose[1]],nodes[1,relPose[0],relPose[1]],'o',label='Vessel Position',color='brown')

#plottting the landmark position in the NEDCordinates 
#plt.plot(NEDlandmarks_x,NEDlandmarks_y,'ro')
#plotting the nearest point in the grid to the real position of the landmarks
plt.plot(nodes[0,list_obstacls[:][0],list_obstacls[:][1]],nodes[1,list_obstacls[:][0],list_obstacls[:][1]],'ro',label='Obstacles')
#plotting the neighbours to the landmarks.
plt.plot(nodes[0,list_neighbors[:][0],list_neighbors[:][1]],nodes[1,list_neighbors[:][0],list_neighbors[:][1]],'o',label='Neighbours to Obstacles',color='orange')
# plotting the path to the exit gate if the path exists 
# if solution_found:
plt.plot(nodes[0,index_of_Path[:,0],index_of_Path[:,1]],nodes[1,index_of_Path[:,0],index_of_Path[:,1]],label='Path',color='blue')
# else:
	# print 'No Solution Found'

if check_path_exists>0:
	plt.plot(trajectory[:,0],trajectory[:,1],label='Path',color='yellow')




#plottting the nearest point in the grid to the real position of the exit gate 
plt.plot(nodes[0,targetPt[0],targetPt[1]],nodes[1,targetPt[0],targetPt[1]],'go',label='Target Position')


plt.xlabel('North Cordinates')
plt.ylabel('East Cordinates')
plt.title('Map Grid')
plt.grid(True)

# plt.legend(label=['Nodes','Vessel Position','Target Position','Obstacles','Neighbours to Obstacles'])
plt.show()